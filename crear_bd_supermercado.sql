-- CREATE TABLE "Cliente" --------------------------------------
CREATE TABLE `Cliente`( 
    `cedula` VARCHAR( 255 ) NOT NULL,
	`nombre` VARCHAR( 255 ) NOT NULL,
	`apellido` VARCHAR( 255 ) NOT NULL,
	`email` VARCHAR( 255 ) NOT NULL,
	`Cliente_id` INT( 255 ) AUTO_INCREMENT NOT NULL,
	PRIMARY KEY ( `Cliente_id` ),
	CONSTRAINT `unique_cedula` UNIQUE( `cedula` ) )
ENGINE = INNODB;
-- -------------------------------------------------------------

-- CREATE TABLE "Productos" ------------------------------------
CREATE TABLE `Productos`( 
    `id` VARCHAR( 255 ) NOT NULL,
	`nombre` VARCHAR( 255 ) NOT NULL,
	`precio` INT( 255 ) NOT NULL,
	`Productos_id` INT( 255 ) NOT NULL,
	PRIMARY KEY ( `Productos_id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
ENGINE = INNODB;
-- -------------------------------------------------------------
-- CREATE TABLE "Factura" --------------------------------------
CREATE TABLE `Factura`( 
    `Factura_id` INT( 255 ) AUTO_INCREMENT NOT NULL,
	`fecha` DATE NOT NULL,
	`Cliente_id` INT( 255 ) NOT NULL,
	PRIMARY KEY ( `Factura_id` ) )
ENGINE = INNODB;
-- -------------------------------------------------------------

-- CREATE INDEX "index_Cliente_id" -----------------------------
CREATE INDEX `index_Cliente_id` ON `Factura`( `Cliente_id` );
-- -------------------------------------------------------------

-- CREATE TABLE "Detalle" --------------------------------------
CREATE TABLE `Detalle`( 
    `id` INT( 255 ) AUTO_INCREMENT NOT NULL,
	`Factura_id` INT( 255 ) NOT NULL,
	`Productos_id` INT( 255 ) NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
ENGINE = INNODB;
-- -------------------------------------------------------------

-- CREATE INDEX "index_Factura_id" -----------------------------
CREATE INDEX `index_Factura_id` ON `Detalle`( `Factura_id` );
-- -------------------------------------------------------------

-- CREATE INDEX "index_Productos_id" ---------------------------
CREATE INDEX `index_Productos_id` ON `Detalle`( `Productos_id` );
-- -------------------------------------------------------------


