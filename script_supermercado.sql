/*
--Mostrar tablas
show tables;

--Ver columnas de una tabla
describe cliente;


INSERT INTO Cliente(cedula, nombre, apellido, email) VALUES("987654310", "Martha", "Hernan", "martha@gmail.com");


#Obtener todos los registros de la tabla
select * from cliente;

#Obtener un registro por la cedula -> where: permite añadir condicionales/filtros a la consulta
select * from cliente where cedula = "0987654" OR cedula = "123456789";

#Obtener registros con una palabra en común en la columna nombre
select * from cliente where nombre like "M%";

select * from cliente where nombre like "%n";

select * from cliente where nombre like "%a%";
*/
SELECT * FROM CLIENTE;
#Actualizar

UPDATE CLIENTE SET EMAIL = "andresupb@gmail.com", NOMBRE = "Dario", APELLIDO = "Peña"
WHERE CEDULA = "1144123456";

#Eliminar un registro
DELETE FROM CLIENTE WHERE CEDULA = "1144123456";

/*

1) Inserte cuatro productos en la base de datos
2) Genere una factura para un cliente
3) Relacione la factura del cliente con el detalle de un producto

*/




